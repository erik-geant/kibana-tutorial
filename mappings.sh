curl -XPUT \
    http://localhost:9200/shakespeare?pretty \
    -H 'Content-Type: application/json' \
    -d '{
 "mappings": {
  "doc": {
   "properties": {
    "speaker": {"type": "keyword"},
    "play_name": {"type": "keyword"},
    "line_id": {"type": "integer"},
    "speech_number": {"type": "integer"}
   }
  }
 }
}'

curl -XPUT \
    http://localhost:9200/logstash-2015.05.18?pretty \
    -H 'Content-Type: application/json' \
    -d '{
  "mappings": {
    "log": {
      "properties": {
        "geo": {
          "properties": {
            "coordinates": {
              "type": "geo_point"
            }
          }
        }
      }
    }
  }
}'

curl -XPUT \
    http://localhost:9200/logstash-2015.05.19?pretty \
    -H 'Content-Type: application/json' \
    -d '{
  "mappings": {
    "log": {
      "properties": {
        "geo": {
          "properties": {
            "coordinates": {
              "type": "geo_point"
            }
          }
        }
      }
    }
  }
}'

curl -XPUT \
    http://localhost:9200/logstash-2015.05.20?pretty \
    -H 'Content-Type: application/json' \
    -d '{
  "mappings": {
    "log": {
      "properties": {
        "geo": {
          "properties": {
            "coordinates": {
              "type": "geo_point"
            }
          }
        }
      }
    }
  }
}'

