curl -XPOST \
 http://localhost:9200/bank/account/_bulk?pretty \
 -H 'Content-Type: application/x-ndjson' \
 --data-binary @accounts.json

curl -XPOST \
 http://localhost:9200/shakespeare/doc/_bulk?pretty \
 -H 'Content-Type: application/x-ndjson' \
 --data-binary @shakespeare_6.0.json

curl -XPOST \
 http://localhost:9200/_bulk?pretty \
 -H 'Content-Type: application/x-ndjson' \
 --data-binary @logs.jsonl
